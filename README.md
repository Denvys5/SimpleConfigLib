# SimpleConfigLib

Simple library to create configuration files for your software

Usage:

Initialize config
```java
Config config = ConfigFactory.getConfig(String <filename>);
```

Add configurable variable. This will load value from config, if it exists. And it will setup configuration with default value.
```java
String <variable> = config.addPropertyString(String <propertyName>, String <propertyDefault>);
```


## Maven

```xml
  <repositories>
    <repository>
      <id>denvys5-artifactory</id>
      <url>https://denvys5.jfrog.io/artifactory/libs-release-local/</url>
    </repository>
  </repositories>


  <dependencies>
    <dependency>
      <groupId>com.denvys5</groupId>
      <artifactId>simple-config-lib</artifactId>
      <version>3.3.3</version>
    </dependency>
  </dependencies>
```

## NEW!
Added support for JSON based configuration! 
Now, you can save and read JSONs from files.

Usage:
Importing object from file
```java
<T> object = getJsonObject(String <absolutePath>, Class<T> <target>);
```
Saving object to file
```java
ConfigFactory.saveJsonObject(String <absolutePath>, Object <object>);
```
Where <T> is your object class type.

Under the hood you will find GSON implementation for JSON serialization and deserialization.

## MAJOR OVERHAUL 3.1 (not directly compatible with previous versions)