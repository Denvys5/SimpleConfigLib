/*
 *  Copyright (C) 2020  Denys Vysoven
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.denvys5.config;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.util.Objects;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

@Slf4j
public class JsonReaderTest {

    @Test
    public void deserializeObject() {
        TestClass1 testClass1 = new TestClass1(new TestClass2("Hello"));
        Gson gson = new GsonBuilder().setPrettyPrinting().create();

        String sample = gson.toJson(testClass1);
        log.info(sample);

        TestClass1 testClass11 = gson.fromJson(sample, TestClass1.class);
        log.info(testClass11.toString());

        assertNotEquals(testClass1.getValue(), testClass11.getValue());

        testClass11.setValue(JsonReader.deserializeObject(testClass11.getValue(), TestClass2.class));
        log.info(testClass11.toString());

        assertEquals(testClass1.getValue(), testClass11.getValue());
    }

    static class TestClass1{
        public Object value;

        public void setValue(Object value) {
            this.value = value;
        }

        public TestClass1(Object value) {
            this.value = value;
        }

        public Object getValue() {
            return value;
        }

        @Override
        public String toString() {
            return "TestClass1{" +
                    "value=" + value +
                    '}';
        }
    }
    static class TestClass2{
        public String hello;

        public TestClass2(String hello) {
            this.hello = hello;
        }

        @Override
        public String toString() {
            return "TestClass2{" +
                    "hello='" + hello + '\'' +
                    '}';
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            TestClass2 that = (TestClass2) o;
            return hello.equals(that.hello);
        }

        @Override
        public int hashCode() {
            return Objects.hash(hello);
        }
    }
}
