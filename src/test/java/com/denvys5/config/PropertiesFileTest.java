/*
 *  Copyright (C) 2020  Denys Vysoven
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.denvys5.config;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class PropertiesFileTest {

    private PropertiesConfig config;

    @Test
    public void existingFileTest() {
        String filename = "test";
        config = ConfigFactory.getPropertiesConfig(filename);
        String propertyName = "property";

        assertTrue(config.isSetProperty(propertyName));
    }

    @Test
    public void nonExistingPropertyTest(){
        String filename = "test";
        config = ConfigFactory.getPropertiesConfig(filename);
        String propertyName = "propertyString1";

        assertFalse(config.isSetProperty(propertyName));
    }

    @Test
    public void nonExistingPropertyValueTest(){
        String filename = "test";
        config = ConfigFactory.getPropertiesConfig(filename);
        String propertyName = "propertyString1";
        String propertyValue = "String";

        assertNotEquals(propertyValue, config.getPropertyString(propertyName));
    }

    @Test
    public void nonExistingPropertyDefaultValueTest(){
        String filename = "test";
        config = ConfigFactory.getPropertiesConfig(filename);
        String propertyName = "propertyString1";
        String propertyValue = "String";

        assertNotEquals(propertyValue, config.getPropertyString(propertyName, propertyValue));
    }

    @Test
    public void nonExistingFileTest() {
        String filename = "test1";
        config = ConfigFactory.getPropertiesConfig(filename);
        String propertyName = "property";

        assertFalse(config.isSetProperty(propertyName));
    }

    @Test
    public void nonExistingFilePropertyTest(){
        String filename = "test1";
        config = ConfigFactory.getPropertiesConfig(filename);
        String propertyName = "propertyString";
        String propertyValue = "String";

        assertNotEquals(propertyValue, config.getPropertyString(propertyName));
    }

    @Test
    public void nonExistingFileDefaultPropertyTest(){
        String filename = "test1";
        config = ConfigFactory.getPropertiesConfig(filename);
        String propertyName = "propertyString";
        String propertyValue = "String";

        assertNotEquals(propertyValue, config.getPropertyString(propertyName, propertyValue));
    }
}
