/*
 *  Copyright (C) 2020  Denys Vysoven
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.denvys5.config;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.Map;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

public class FileConfigTest {
    private FileConfig config;
    private String filename = "configuration.cfg";

    @BeforeEach
    public void setUp() {
        File file = new File(filename);
        file.delete();
        config = ConfigFactory.getConfig(filename);
    }

    @Test
    public void addPropertyString() {
        String propertyName = "property";
        String propertyDefault = "B";
        config.addPropertyString(propertyName, propertyDefault);

        assertEquals(propertyDefault, config.getPropertyString(propertyName));
    }

    @Test
    public void addPropertyBoolean() {
        String propertyName = "property";
        boolean propertyDefault = true;
        config.addPropertyBoolean(propertyName, propertyDefault);

        assertEquals(propertyDefault, config.getPropertyBoolean(propertyName));
    }

    @Test
    public void addPropertyInt() {
        String propertyName = "property";
        int propertyDefault = 10;
        config.addPropertyInt(propertyName, propertyDefault);

        assertEquals(propertyDefault, config.getPropertyInt(propertyName));
    }

    @Test
    public void addPropertyDouble() {
        String propertyName = "property";
        double propertyDefault = 10D;
        config.addPropertyDouble(propertyName, propertyDefault);
        assertTrue(Math.abs(propertyDefault - config.getPropertyDouble(propertyName)) < 0.001);
    }

    @Test
    public void isSetProperty() {
        String propertyName = "property";
        double propertyDefault = 10D;
        config.addPropertyDouble(propertyName, propertyDefault);
        assertTrue(config.isSetProperty(propertyName));
    }

    @Test
    public void setProperty() {
        String propertyName = "property";
        int propertyDefault = 10;
        config.setProperty(propertyName, propertyDefault);

        assertEquals(propertyDefault, config.getPropertyInt(propertyName));


        propertyDefault = 20;
        config.setProperty(propertyName, propertyDefault);

        assertEquals(propertyDefault, config.getPropertyInt(propertyName));
    }

    @Test
    public void getPropertyInt() {
        String propertyName = "property";
        int propertyDefault = 10;
        config.addPropertyInt(propertyName, propertyDefault);

        assertEquals(propertyDefault, config.getPropertyInt(propertyName));
    }

    @Test
    public void testGetPropertyInt() {
        String propertyName = "property";
        int propertyDefault = 10;

        assertEquals(propertyDefault, config.getPropertyInt(propertyName, propertyDefault));
    }

    @Test
    public void getPropertyBoolean() {
        String propertyName = "property";
        boolean propertyDefault = true;
        config.addPropertyBoolean(propertyName, propertyDefault);

        assertEquals(propertyDefault, config.getPropertyBoolean(propertyName));
    }

    @Test
    public void testGetPropertyBoolean() {
        String propertyName = "property";
        boolean propertyDefault = true;

        assertEquals(propertyDefault, config.getPropertyBoolean(propertyName, propertyDefault));
    }

    @Test
    public void getPropertyString() {
        String propertyName = "property";
        String propertyDefault = "true";
        config.addPropertyString(propertyName, propertyDefault);

        assertEquals(propertyDefault, config.getPropertyString(propertyName));
    }

    @Test
    public void testGetPropertyString() {
        String propertyName = "property";
        String propertyDefault = "true";

        assertEquals(propertyDefault, config.getPropertyString(propertyName, propertyDefault));
    }

    @Test
    public void getPropertyDouble() {
        String propertyName = "property";
        double propertyDefault = 10D;
        config.addPropertyDouble(propertyName, propertyDefault);

        assertTrue(Math.abs(propertyDefault - config.getPropertyDouble(propertyName)) < 0.001);
    }

    @Test
    public void testGetPropertyDouble() {
        String propertyName = "property";
        double propertyDefault = 10D;

        assertTrue(Math.abs(propertyDefault - config.getPropertyDouble(propertyName, propertyDefault)) < 0.001);
    }

    @Test
    public void refresh() {
        String propertyName = "property";
        String propertyDefault = "true";
        String propertyChanged = "false";
        config.addPropertyString(propertyName, propertyDefault);

        FileConfig second = ConfigFactory.getConfig(filename);

        assertEquals(propertyDefault, config.getPropertyString(propertyName));
        assertEquals(propertyDefault, second.getPropertyString(propertyName));
        assertEquals(second.getPropertyString(propertyName), config.getPropertyString(propertyName));

        second.setProperty(propertyName, propertyChanged);

        assertEquals(propertyDefault, config.getPropertyString(propertyName));
        assertEquals(propertyChanged, second.getPropertyString(propertyName));
        assertNotEquals(second.getPropertyString(propertyName), config.getPropertyString(propertyName));

        config.refresh();

        assertEquals(propertyChanged, config.getPropertyString(propertyName));
        assertEquals(propertyChanged, second.getPropertyString(propertyName));
        assertEquals(second.getPropertyString(propertyName), config.getPropertyString(propertyName));
    }

    @Test
    public void getMap() {
        String propertyName = "property";
        String propertyDefault = "true";
        config.addPropertyString(propertyName, propertyDefault);

        Map<String, String> map = config.getMap();

        assertEquals(1, map.size());
        assertTrue(map.containsKey(propertyName));
        assertTrue(map.containsValue(propertyDefault));
    }

    @Test
    public void getKeys() {
        String propertyName = "property";
        String propertyDefault = "true";
        config.addPropertyString(propertyName, propertyDefault);

        Set<String> set = config.getKeys();

        assertEquals(1, set.size());
        assertTrue(set.contains(propertyName));
    }

    @Test
    public void removeProperty() {
        String propertyName = "property";
        String propertyDefault = "true";
        config.addPropertyString(propertyName, propertyDefault);

        assertEquals(propertyDefault, config.getPropertyString(propertyName));

        config.removeProperty(propertyName);

        assertNotEquals(propertyDefault, config.getPropertyString(propertyName));
    }
}
