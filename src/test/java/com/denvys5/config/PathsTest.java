/*
 *  Copyright (C) 2020  Denys Vysoven
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.denvys5.config;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.assertEquals;

@Slf4j
public class PathsTest {
    private final String filename = "test.json";
    private final TestJson testJson = new TestJson();

    private final Gson gson = new GsonBuilder().setPrettyPrinting().create();

    @Test
    public void testRelative1() throws IOException {
        Path path = Paths.get(filename);
        testCase(path);
    }

    @Test
    public void testRelative2() throws IOException {
        Path path = new File(Utils.getRelativeFilepath() + filename).toPath();
        testCase(path);
    }

    @Test
    public void testRelative3() throws IOException {
        Path path = new File("./"+filename).toPath();
        testCase(path);
    }

    private void testCase(Path path) throws IOException {
        log.info(path.toAbsolutePath().toString());

        Writer out = Files.newBufferedWriter(path);
        out.write(gson.toJson(testJson));
        out.flush();


        BufferedReader in = Files.newBufferedReader(Paths.get(filename));
        StringBuilder stringBuilder = new StringBuilder();
        in.lines().forEach(stringBuilder::append);
        log.info(stringBuilder.toString());
        assertEquals(testJson, gson.fromJson(stringBuilder.toString(), TestJson.class));
    }


    static class TestJson{
        String first = "Hello World";
        List<String> words = new ArrayList<>();

        public TestJson() {
            this.words.addAll(Arrays.asList("Hello", "World", "!"));
        }

        @Override
        public String toString() {
            return "TestJson{" +
                    "first='" + first + '\'' +
                    ", words=" + words +
                    '}';
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            TestJson testJson = (TestJson) o;
            return Objects.equals(first, testJson.first) &&
                    Objects.equals(words, testJson.words);
        }

        @Override
        public int hashCode() {
            return Objects.hash(first, words);
        }
    }
}
