/*
 *  Copyright (C) 2020  Denys Vysoven
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.denvys5.config;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;

import static org.junit.jupiter.api.Assertions.assertEquals;

@Slf4j
public class ConfigFactoryTest {
    private final String filename = "configuration.cfg";

    @BeforeEach
    public void setUp() {
        File file = new File(filename);
        file.delete();
    }

    @Test
    public void testFilePath(){
        log.info("testFilePath");
        log.info(Utils.getRelativeFilepath());
    }

    @Test
    public void testGetConfig1() {
        log.info("testGetConfig1");
        String absolutePath = Utils.getRelativeFilepath();
        log.info(absolutePath);
        testCase(ConfigFactory.getConfig(absolutePath, filename));
    }

    @Test
    public void testGetConfig2() {
        log.info("testGetConfig2");
        testCase(ConfigFactory.getConfig(filename));
    }

    @Test
    public void testGetConfig3() {
        log.info("testGetConfig3");
        testCase(ConfigFactory.getConfig());
    }

    @Test
    public void getConfigWithRelativeFilePath() {
        log.info("getConfigWithRelativeFilePath");
        testCase(ConfigFactory.getConfigWithApproximateRelativeFilePath("", filename));
    }

    private void testCase(FileConfig config){
        config.setProperty("hello", "world");
        config.setProperty("hello", "world2");
        config.setProperty("hell0", "world2");
        config.setProperty("hEllo", "world3");
        config.setProperty("123", 129);

        assertEquals(130, config.getPropertyInt("124", 130));
        assertEquals(129, config.getPropertyInt("123", 130));
        assertEquals("world3", config.getPropertyString("hEllo"));
    }
}
