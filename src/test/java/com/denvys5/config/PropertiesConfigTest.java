/*
 *  Copyright (C) 2020  Denys Vysoven
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.denvys5.config;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class PropertiesConfigTest {
    private PropertiesConfig config;

    @BeforeEach
    public void setUp() throws Exception {
        String filename = "test";
        config = ConfigFactory.getPropertiesConfig(filename);
    }

    @Test
    public void isSetProperty() {
        String propertyName = "property";

        assertTrue(config.isSetProperty(propertyName));
    }

    @Test
    public void getPropertyInt() {
        String propertyName = "propertyInt";
        int propertyDefault = 10;

        assertEquals(propertyDefault, config.getPropertyInt(propertyName));
    }

    @Test
    public void testGetPropertyInt() {
        String propertyName = "propertyInt";
        int propertyDefault = 10;

        assertEquals(propertyDefault, config.getPropertyInt(propertyName, propertyDefault));
    }

    @Test
    public void getPropertyBoolean() {
        String propertyName = "propertyBool";
        boolean propertyDefault = true;

        assertEquals(propertyDefault, config.getPropertyBoolean(propertyName));
    }

    @Test
    public void testGetPropertyBoolean() {
        String propertyName = "propertyBool";
        boolean propertyDefault = true;

        assertEquals(propertyDefault, config.getPropertyBoolean(propertyName, propertyDefault));
    }

    @Test
    public void getPropertyString() {
        String propertyName = "propertyString";
        String propertyDefault = "String";

        assertEquals(propertyDefault, config.getPropertyString(propertyName));
    }

    @Test
    public void testGetPropertyString() {
        String propertyName = "propertyString";
        String propertyDefault = "String";

        assertEquals(propertyDefault, config.getPropertyString(propertyName, propertyDefault));
    }

    @Test
    public void getPropertyDouble() {
        String propertyName = "propertyDouble";
        double propertyDefault = 10D;

        assertTrue(Math.abs(propertyDefault - config.getPropertyDouble(propertyName)) < 0.001);
    }

    @Test
    public void testGetPropertyDouble() {
        String propertyName = "propertyDouble";
        double propertyDefault = 10D;

        assertTrue(Math.abs(propertyDefault - config.getPropertyDouble(propertyName, propertyDefault)) < 0.001);
    }
}
