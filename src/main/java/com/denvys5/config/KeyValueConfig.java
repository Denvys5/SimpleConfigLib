/*
 *  Copyright (C) 2020  Denys Vysoven
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.denvys5.config;

import java.util.Map;
import java.util.Set;

public interface KeyValueConfig {
    Map<String, String> getMap();

    Set<String> getKeys();

    boolean isSetProperty(String key);

    int getPropertyInt(String s);

    int getPropertyInt(String s, int d);

    boolean getPropertyBoolean(String s);

    boolean getPropertyBoolean(String s, boolean b);

    String getPropertyString(String s);

    String getPropertyString(String s, String str);

    double getPropertyDouble(String s);

    double getPropertyDouble(String s, double d);
}
