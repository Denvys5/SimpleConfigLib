/*
 *  Copyright (C) 2020  Denys Vysoven
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.denvys5.config;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.*;
import java.util.stream.Collectors;

public class PropertiesConfig implements KeyValueConfig {
    private Utf8ResourceBundle resourceBundle;
    private static final ResourceBundle.Control control = new Utf8ResourceBundle.UTF8Control();

    PropertiesConfig(String filename) {
        try {
            resourceBundle = new Utf8ResourceBundle(ResourceBundle.getBundle(filename, Locale.ROOT, control));
        }catch (MissingResourceException e){
            resourceBundle = new EmptyUTF8ResourceBundle();
        }
    }

    @Override
    public boolean isSetProperty(String key) {
        return resourceBundle.getKeySet().contains(key);
    }

    @Override
    public int getPropertyInt(String s) {
        if (isSetProperty(s)) {
            try {
                return Integer.parseInt(getPropertyString(s));
            }catch (NumberFormatException ignored){}
        }
        return 0;
    }

    @Override
    public int getPropertyInt(String s, int d) {
        if (isSetProperty(s)) {
            try {
                return Integer.parseInt(getPropertyString(s));
            }catch (NumberFormatException ignored){}
        }
        return d;
    }

    @Override
    public boolean getPropertyBoolean(String s) {
        if (isSetProperty(s)) {
            return Boolean.parseBoolean(getPropertyString(s));
        }
        return false;
    }

    @Override
    public boolean getPropertyBoolean(String s, boolean b) {
        if (isSetProperty(s)) {
            return Boolean.parseBoolean(getPropertyString(s));
        }
        return b;
    }

    @Override
    public String getPropertyString(String s) {
        return getString(s);
    }

    @Override
    public String getPropertyString(String s, String str) {
        if (isSetProperty(s)) return getPropertyString(s);
        return null;
    }

    @Override
    public double getPropertyDouble(String s) {
        if (isSetProperty(s)) {
            try {
                return Double.parseDouble(getPropertyString(s));
            }catch (NumberFormatException ignored){}
        }
        return 0;
    }

    @Override
    public double getPropertyDouble(String s, double d) {
        if (isSetProperty(s)) {
            try {
                return Double.parseDouble(getPropertyString(s));
            }catch (NumberFormatException ignored){}
        }
        return d;
    }


    /**
     *
     * @param key key of the resource to fetch
     * @return fetched string or error message otherwise
     */
    public String getString(String key) {
        String result;
        try {
            result = resourceBundle.getValue(key);
        } catch (MissingResourceException e) {
            result = null;
        }

        return result;
    }

    @Override
    public Map<String, String> getMap() {
        return resourceBundle.getKeySet()
                .stream()
                .collect(Collectors.toMap((e) -> e,
                        (e) -> resourceBundle.getValue(e)));
    }

    public Set<String> getKeys() {
        return resourceBundle.getKeySet();
    }

    private static class Utf8ResourceBundle extends ResourceBundle {

        private static final String BUNDLE_EXTENSION = "properties";
        private static final String CHARSET = "UTF-8";

        Utf8ResourceBundle(ResourceBundle resourceBundle) {
            setParent(resourceBundle);
        }

        private Utf8ResourceBundle() {
        }

        protected String getValue(String key) throws MissingResourceException{
            return getString(key);
        }

        @Override
        protected Object handleGetObject(String key) {
            return parent.getObject(key);
        }

        @Override
        public Enumeration<String> getKeys() {
            return parent.getKeys();
        }

        public Set<String> getKeySet() {
            return parent.keySet();
        }

        private static class UTF8Control extends Control {
            public ResourceBundle newBundle(String baseName,
                                            Locale locale,
                                            String format,
                                            ClassLoader loader,
                                            boolean reload) throws IOException {
                String bundleName = toBundleName(baseName, locale);
                String resourceName = toResourceName(bundleName, BUNDLE_EXTENSION);
                ResourceBundle bundle = null;
                InputStream stream = null;
                if (reload) {
                    URL url = loader.getResource(resourceName);
                    if (url != null) {
                        URLConnection connection = url.openConnection();
                        if (connection != null) {
                            connection.setUseCaches(false);
                            stream = connection.getInputStream();
                        }
                    }
                } else {
                    stream = loader.getResourceAsStream(resourceName);
                }
                if (stream != null) {
                    try {
                        bundle = new PropertyResourceBundle(new InputStreamReader(stream, CHARSET));
                    } finally {
                        stream.close();
                    }
                }
                return bundle;
            }
        }
    }

    private static class EmptyUTF8ResourceBundle extends Utf8ResourceBundle {
        protected String getValue(String key) {
            return null;
        }

        @Override
        protected Object handleGetObject(String key) {
            return null;
        }

        @Override
        public Enumeration<String> getKeys() {
            return Collections.emptyEnumeration();
        }

        public Set<String> getKeySet() {
            return Collections.emptySet();
        }
    }
}
