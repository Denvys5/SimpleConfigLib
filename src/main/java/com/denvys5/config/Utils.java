/*
 *  Copyright (C) 2020  Denys Vysoven
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.denvys5.config;

import org.scijava.util.FileUtils;
import org.scijava.util.Types;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.nio.file.Paths;
import java.util.Arrays;

class Utils {
    static String getRelativeFilepath(){
        String relative = Paths.get("./").toFile().getAbsolutePath();
        if(relative.endsWith(".")) relative = relative.substring(0, relative.length()-1);
        return relative;
    }

    static String getRelativeFilepath(Class<?> mainClass){
        File relativeToJar = FileUtils.urlToFile(Types.location(mainClass));
        if(relativeToJar.getAbsolutePath().endsWith(".jar")){
            File folder = relativeToJar.getParentFile();
            return folder.getAbsolutePath() + File.separator;
        }else if(relativeToJar.isDirectory()){
            return relativeToJar.getAbsolutePath() + File.separator;
        }else{
            return getRelativeFilepath();
        }
    }

    static String getFileNameFromCanonicalPath(String filepath){
        String fileName;
        int separatorIndex = Arrays.stream(
                new int[]{filepath.lastIndexOf('\\'),
                        filepath.lastIndexOf('/'),
                        filepath.lastIndexOf(File.separator)
                })
                .max()
                .getAsInt();
        if(separatorIndex != -1){
            fileName = filepath.substring(separatorIndex);
        }else{
            fileName = filepath;
        }
        return fileName;
    }

    static File getFile(String filePath, String fileName) {
        if(filePath.endsWith("\\") || filePath.endsWith("/") || filePath.endsWith(File.separator)){
            return new File(filePath + fileName);
        }else{
            return new File(filePath + File.separator + fileName);
        }
    }

    static void writeToFile(InputStream input, File out) {
        if (input != null) {
            FileOutputStream output = null;
            try {
                out.getParentFile().mkdirs();
                output = new FileOutputStream(out);
                byte[] buf = new byte[8192];
                int length;
                while ((length = input.read(buf)) > 0) {
                    output.write(buf, 0, length);
                }
            } catch (Exception ignored) {
            } finally {
                try {
                    input.close();
                } catch (Exception ignored) {
                }
                try {
                    if (output != null)
                        output.close();
                } catch (Exception ignored) {
                }
            }
        }
    }
}
