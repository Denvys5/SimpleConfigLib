/*
 *  Copyright (C) 2020  Denys Vysoven
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.denvys5.config;

public interface EditableKeyValueConfig extends KeyValueConfig{
    void refresh();

    String addPropertyString(final String propertyName, final String defaultValue);

    boolean addPropertyBoolean(final String propertyName, final boolean defaultValue);

    int addPropertyInt(final String propertyName, final int defaultValue);

    double addPropertyDouble(final String propertyName, final double defaultValue);

    void setProperty(String s, Object value);

    void removeProperty(String s);
}
