/*
 *  Copyright (C) 2020  Denys Vysoven
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.denvys5.config;

import lombok.extern.slf4j.Slf4j;

import java.io.*;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
public class FileConfig implements EditableKeyValueConfig {
    private final FileConfigCore config;

    FileConfig(String filePath, String fileName) {
        config = new FileConfigCore(fileName, Utils.getFile(filePath, fileName));
        config.load();
    }

    FileConfig(String file) {
        config = new FileConfigCore(Utils.getFileNameFromCanonicalPath(file), new File(file));
        config.load();
    }

    FileConfig(File file) {
        config = new FileConfigCore(file.getName(), file);
        config.load();
    }

    @Override
    public void refresh() {
        config.refreshCache();
    }

    @Override
    public String addPropertyString(final String propertyName, final String defaultValue){
        if(!config.checkProperty(propertyName)) {
            config.put(propertyName, defaultValue);
            return defaultValue;
        }

        return config.getPropertyString(propertyName);
    }

    @Override
    public boolean addPropertyBoolean(final String propertyName, final boolean defaultValue){
        if(!config.checkProperty(propertyName)) {
            config.put(propertyName, defaultValue);
            return defaultValue;
        }

        return config.getPropertyBoolean(propertyName);
    }

    @Override
    public int addPropertyInt(final String propertyName, final int defaultValue){
        if(!config.checkProperty(propertyName)) {
            config.put(propertyName, defaultValue);
            return defaultValue;
        }

        return config.getPropertyInteger(propertyName);
    }

    @Override
    public double addPropertyDouble(final String propertyName, final double defaultValue){
        if(!config.checkProperty(propertyName)) {
            config.put(propertyName, defaultValue);
            return defaultValue;
        }

        return config.getPropertyDouble(propertyName);
    }

    @Override
    public Map<String, String> getMap() {
        return config.getProperties().stream().collect(Collectors.toMap((e) -> e, config::getPropertyString));
    }

    @Override
    public Set<String> getKeys() {
        return config.getProperties();
    }

    @Override
    public boolean isSetProperty(String key){
        return config.checkProperty(key);
    }

    @Override
    public void setProperty(String s, Object value) {
        if (config.checkProperty(s)) config.changeProperty(s, value);
        else config.put(s, value);
    }

    @Override
    public void removeProperty(String s) {
        if (!config.checkProperty(s)) return;
        config.removeProperty(s);
    }

    @Override
    public int getPropertyInt(String s) {
        if (config.checkProperty(s)) return config.getPropertyInteger(s);
        return 0;
    }

    @Override
    public int getPropertyInt(String s, int d) {
        if (config.checkProperty(s)) return config.getPropertyInteger(s);
        return d;
    }

    @Override
    public boolean getPropertyBoolean(String s) {
        if (config.checkProperty(s)) return config.getPropertyBoolean(s);
        return false;
    }

    @Override
    public boolean getPropertyBoolean(String s, boolean b) {
        if (config.checkProperty(s)) return config.getPropertyBoolean(s);
        return b;
    }

    @Override
    public String getPropertyString(String s) {
        if (config.checkProperty(s)) return config.getPropertyString(s);
        return null;
    }

    @Override
    public String getPropertyString(String s, String str) {
        if (config.checkProperty(s)) return config.getPropertyString(s);
        return str;
    }

    @Override
    public double getPropertyDouble(String s){
        if (config.checkProperty(s)) return config.getPropertyDouble(s);
        return 0D;
    }

    @Override
    public double getPropertyDouble(String s, double d) {
        if (config.checkProperty(s)) return config.getPropertyDouble(s);
        return d;
    }


    private static class FileConfigCore {
        private final Object lock = new Object();
        private final File file;
        private boolean cached = false;
        private final String filename;
        private Map<String, String> cache;

        public FileConfigCore(String filename, File file) {
            this.filename = filename;
            this.file = file;
            setCached(false);
        }

        public boolean isCached() {
            return cached;
        }

        public void refreshCache(){
            synchronized (lock){
                cache = loadHashMap();
                setCached(true);
            }
        }

        public void setCached(boolean cached) {
            this.cached = cached;
            if (!cached)
                cache = Collections.emptyMap();
        }

        private void create(String filename) {
            InputStream input = getClass().getResourceAsStream(filename);
            Utils.writeToFile(input, file);
        }

        private HashMap<String, String> loadHashMap() {
            HashMap<String, String> result = new HashMap<>();
            try (BufferedReader br = new BufferedReader(new FileReader(file))) {
                String line;
                while ((line = br.readLine()) != null) {
                    if ((line.isEmpty()) || (line.startsWith("#")) || (!line.contains(": "))) continue;
                    String[] args = line.split(": ");
                    if (args.length < 2) {
                        result.put(args[0], null);
                        continue;
                    }
                    result.put(args[0], args[1]);
                }
            } catch (IOException ignored) {
            }
            return result;
        }

        public void load() {
            if (filename != null && !file.exists()) create(filename);
            refreshCache();
        }

        public Set<String> getProperties(){
            try {
                if (!cached) refreshCache();
                return cache.keySet();
            } catch (Exception ignored) {
            }
            return Collections.emptySet();
        }

        public String getPropertyString(String property) {
            try {
                if (!cached) refreshCache();
                return cache.get(property);
            } catch (Exception ignored) {
            }
            return null;
        }

        public int getPropertyInteger(String property) {
            try {
                if (!cached) refreshCache();
                return Integer.parseInt(cache.get(property));
            } catch (Exception ignored) {
            }
            return 0;
        }

        public boolean getPropertyBoolean(String property) {
            try {
                if (!cached) refreshCache();
                return Boolean.parseBoolean(cache.get(property));
            } catch (Exception ignored) {
            }
            return false;
        }

        public double getPropertyDouble(String property) {
            try {
                if (!cached) refreshCache();
                String result = cache.get(property);
                if (result.isEmpty()) result += ".0";
                return Double.parseDouble(result);
            } catch (Exception ignored) {
            }
            return 0;
        }

        public boolean checkProperty(String property) {
            try {
                if (!cached) refreshCache();
                return cache.containsKey(property);
            } catch (Exception ignored) {
            }
            return false;
        }

        private void flush(Map<Integer, String> newContents) {
            synchronized (lock){
                try {
                    this.delFile(file);
                    file.createNewFile();
                    BufferedWriter writer = new BufferedWriter(new FileWriter(file));
                    for (int i = 1; i <= newContents.size(); i++) {
                        String line = newContents.get(i);
                        if (line == null) {
                            writer.append("\n");
                            continue;
                        }
                        writer.append(line);
                        writer.append("\n");
                    }
                    writer.flush();
                    writer.close();
                } catch (Exception ignored) {
                }
                setCached(false);
            }
        }

        private void delFile(File file) {
            if (file.exists()) file.delete();
        }

        private Map<Integer, String> getAllFileContents() {
            synchronized (lock){
                Map<Integer, String> result = new HashMap<>();
                Integer i = 1;
                try (BufferedReader br = new BufferedReader(new FileReader(file))) {
                    String line;

                    while ((line = br.readLine()) != null) {
                        if (line.isEmpty()) {
                            result.put(i, null);
                            i++;
                            continue;
                        }

                        result.put(i, line);
                        i++;
                    }
                } catch (FileNotFoundException e) {
                    log.warn("Configuration file {} was not found. Creating one.", file.getName());
                } catch (Exception ignored) {
                }
                return result;
            }
        }

        public void changeProperty(String property, Object obj) {
            Map<Integer, String> contents = this.getAllFileContents();
            for (int i = 1; i <= contents.size(); i++) {
                if (contents.get(i) == null) continue;
                String check = contents.get(i);
                if (check.startsWith(property)) {
                    check = check.replace(property, "");
                    if (!(check.startsWith(": "))) continue;
                    contents.remove(i);
                    contents.put(i, property + ": " + obj.toString());
                }
            }
            this.flush(contents);
        }

        public void removeProperty(String property){
            Map<Integer, String> contents = this.getAllFileContents();
            for (int i = 1; i <= contents.size(); i++) {
                if (contents.get(i) == null) continue;
                String check = contents.get(i);
                if (check.startsWith(property)) {
                    check = check.replace(property, "");
                    if (!(check.startsWith(": "))) continue;
                    contents.remove(i);
                }
            }
            this.flush(contents);
        }

        public void put(String property, Object obj) {
            Map<Integer, String> contents = this.getAllFileContents();
            contents.put(contents.size() + 1, property + ": " + obj.toString());
            flush(contents);
        }

        @Deprecated
        public void put(String property, Object obj, Integer line) {
            Map<Integer, String> contents = this.getAllFileContents();
            if (line >= contents.size() + 1) return;
            HashMap<Integer, String> newContents = new HashMap<Integer, String>();
            for (int i = 1; i < line; i++) newContents.put(i, contents.get(i));
            newContents.put(line, property + ": " + obj.toString());
            for (int i = line; i <= contents.size(); i++) newContents.put(i + 1, contents.get(i));
            flush(newContents);
        }

        @Deprecated
        public void insertComment(String comment) {
            Map<Integer, String> contents = this.getAllFileContents();
            contents.put(contents.size() + 1, "#" + comment);
            flush(contents);
        }

        @Deprecated
        public void insertComment(String comment, Integer line) {
            Map<Integer, String> contents = getAllFileContents();
            if (line >= contents.size() + 1) return;
            HashMap<Integer, String> newContents = new HashMap<>();
            for (int i = 1; i < line; i++) newContents.put(i, contents.get(i));
            newContents.put(line, "#" + comment);
            for (int i = line; i <= contents.size(); i++) newContents.put(i + 1, contents.get(i));
            flush(newContents);
        }

        @Deprecated
        public Integer getLineCount() {
            Map<Integer, String> contents = getAllFileContents();
            return contents.size();
        }
    }

}
