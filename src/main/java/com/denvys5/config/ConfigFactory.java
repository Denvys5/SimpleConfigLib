/*
 *  Copyright (C) 2020  Denys Vysoven
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.denvys5.config;

import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.FileNotFoundException;

@Slf4j
public class ConfigFactory{
    /**
     * Opens a {@link File} and returns {@link FileConfig} with data binding to it
     *
     * @param absolutePath absolute path to directory containing a file, e.g. "/var/folder/".
     * @param filename canonical filename, e.g. "settings.cfg"
     * @return {@link FileConfig} that implements {@link EditableKeyValueConfig} with data binding to a selected file
     */
    public static FileConfig getConfig(String absolutePath, String filename){
        log.info("Config path: {}", Utils.getFile(absolutePath, filename).getAbsolutePath());
        return new FileConfig(absolutePath, filename);
    }

    /**
     * Opens a {@link File} and returns {@link FileConfig} with data binding to it
     *
     * @param absolutePath absolute path to file, e.g. "./myfile.cfg".
     * @return {@link FileConfig} that implements {@link EditableKeyValueConfig} with data binding to a selected file
     */
    public static FileConfig getConfig(String absolutePath){
        log.info("Config path: {}", new File(absolutePath).getAbsolutePath());
        return new FileConfig(absolutePath);
    }

    /**
     * Opens a {@link File} and returns {@link FileConfig} with data binding to it
     *
     * @param file file to use
     * @return {@link FileConfig} that implements {@link EditableKeyValueConfig} with data binding to a selected file
     */
    public static FileConfig getConfig(File file){
        log.info("Config path: {}", file.getAbsolutePath());
        return new FileConfig(file);
    }

    /**
     * Opens a {@link File} named "settings.cfg" in approximate relative path root and returns {@link FileConfig} with data binding to it
     *
     * @return {@link FileConfig} that implements {@link EditableKeyValueConfig} with data binding to a selected file
     */
    public static FileConfig getConfig(){
        return getConfig(Utils.getRelativeFilepath(), DefaultParameters.DEFAULT_CONFIG_FILENAME);
    }

    /**
     * Opens a {@link File} and returns {@link FileConfig} with data binding to it
     *
     * @param relativePath relative path to file, e.g. "myfile.json". May not be an actual one, uses default path addressing
     * @return {@link FileConfig} that implements {@link EditableKeyValueConfig} with data binding to a selected file
     */
    public static FileConfig getConfigWithApproximateRelativeFilePath(String relativePath){
        return getConfig(Utils.getRelativeFilepath(), relativePath);
    }

    /**
     * Opens a {@link File} and returns {@link FileConfig} with data binding to it
     *
     * @param relativePath relative path to folder containing a file, e.g. "/myfolder/". May not be an actual one, uses default path addressing
     * @param filename canonical filename, e.g. "settings.cfg"
     * @return {@link FileConfig} that implements {@link EditableKeyValueConfig} with data binding to a selected file
     */
    public static FileConfig getConfigWithApproximateRelativeFilePath(String relativePath, String filename){
        if(relativePath.equals(".")) return getConfigWithApproximateRelativeFilePath(filename);
        return getConfig(Utils.getRelativeFilepath() + relativePath, filename);
    }

    /**
     * Opens a {@link File} and returns {@link FileConfig} with data binding to it
     *
     * @param relativePath relative path to a file, e.g. "/myfolder/myfile.cfg". Returns relative path, based on {@link Class} position in file system
     * @param mainclass class that is used to determine true relative path. You should always specify application`s mainclass to get a proper relative path
     * @return {@link FileConfig} that implements {@link EditableKeyValueConfig} with data binding to a selected file
     */
    public static FileConfig getConfigWithRelativeFilePath(String relativePath, Class<?> mainclass){
        return getConfig(Utils.getRelativeFilepath(mainclass), relativePath);
    }

    /**
     * Opens a {@link File} and returns {@link FileConfig} with data binding to it
     *
     * @param relativePath relative path to a file, e.g. "/myfolder/myfile.cfg". Returns relative path, based on {@link Class} position in file system
     * @param mainclass class that is used to determine true relative path. You should always specify application`s mainclass to get a proper relative path
     * @return {@link FileConfig} that implements {@link EditableKeyValueConfig} with data binding to a selected file
     */
    public static FileConfig getConfigWithRelativeFilePath(String relativePath, String filename, Class<?> mainclass){
        if(relativePath.equals(".")) return getConfigWithRelativeFilePath(filename, mainclass);
        return getConfig(Utils.getRelativeFilepath(mainclass) + relativePath, filename);
    }

    /**
     * Opens a {@link File} and returns {@link FileConfig} with data binding to it
     *
     * @param relativePath relative path to folder containing a file, e.g. "/myfolder/". Returns relative path, based on {@link Class} position in file system
     * @param fileName canonical filename, e.g. "settings.cfg"
     * @param mainclass class that is used to determine true relative path. You should always specify application`s mainclass to get a proper relative path
     * @param relativity boolean parameter, that decides which method of determining relative path to use - true relative path, or approximate one
     * @return {@link FileConfig} that implements {@link EditableKeyValueConfig} with data binding to a selected file
     */
    public static FileConfig getConfigWithSwitchableRelativePath(String relativePath, String fileName, Class<?> mainclass, boolean relativity){
        return relativity ?
                getConfigWithRelativeFilePath(relativePath, fileName, mainclass)
                :
                getConfigWithApproximateRelativeFilePath(relativePath, fileName);
    }

    /**
     * Opens a {@link File} and returns {@link FileConfig} with data binding to it
     *
     * @param relativePath relative path to a file, e.g. "/myfolder/myfile.cfg". Returns relative path, based on {@link Class} position in file system
     * @param mainclass class that is used to determine true relative path. You should always specify application`s mainclass to get a proper relative path
     * @param relativity boolean parameter, that decides which method of determining relative path to use - true relative path, or approximate one
     * @return {@link FileConfig} that implements {@link EditableKeyValueConfig} with data binding to a selected file
     */
    public static FileConfig getConfigWithSwitchableRelativePath(String relativePath, Class<?> mainclass, boolean relativity){
        return relativity ?
                getConfigWithRelativeFilePath(relativePath, mainclass)
                :
                getConfigWithApproximateRelativeFilePath(relativePath);
    }

    /**
     * Opens .properties Java resource and returns {@link PropertiesConfig} with data binding to it
     *
     * @param filename relative path to resource, excluding file extension, e.g. "template/myfile" (but not "myfile.props").
     * @return {@link PropertiesConfig} that implements {@link KeyValueConfig} with data binding to a selected resource
     */
    public static PropertiesConfig getPropertiesConfig(String filename){
        log.info("Properties path: resources{}{}.properties", File.separator, filename);
        return new PropertiesConfig(filename);
    }

    /**
     * Opens a {@link File} and deserializes it into an {@link Object} based on target {@link Class}.
     *
     * @param absolutePath absolute path to file, e.g. "./myfile.json".
     * @param target class that is returned from deserialization.
     * @return An object, that was read from JSON file and deserialized based on the target class
     * @throws FileNotFoundException if the absolutePath does not correspond to a file.
     * @throws JsonSyntaxException if the file is not a proper JSON
     * @throws JsonIOException if the file cannot be deserialized to a target class
     */
    public static <T> T getJsonObject(String absolutePath, Class<T> target)
            throws FileNotFoundException, JsonSyntaxException, JsonIOException {
        log.info("Json path: {}", absolutePath);
        return JsonReader.getObject(absolutePath, target);
    }

    /**
     * Opens a {@link File} and deserializes it into an {@link Object} based on target {@link Class}.
     *
     * @param relativePath relative path to file, e.g. "myfile.json". May not be an actual one, uses default path addressing
     * @param target class that is returned from deserialization.
     * @return An object, that was read from JSON file and deserialized based on the target class
     * @throws FileNotFoundException if the absolutePath does not correspond to a file.
     * @throws JsonSyntaxException if the file is not a proper JSON
     * @throws JsonIOException if the file cannot be deserialized to a target class
     */
    public static <T> T getJsonObjectWithApproximateRelativeFilePath(String relativePath, Class<T> target)
            throws FileNotFoundException, JsonSyntaxException, JsonIOException {
        log.info("Json path: {}", Utils.getRelativeFilepath() + relativePath);
        return JsonReader.getObject(Utils.getRelativeFilepath() + relativePath, target);
    }

    /**
     * Opens a {@link File} and deserializes it into an {@link Object} based on target {@link Class}.
     *
     * @param relativePath relative path to file, e.g. "myfile.json". Returns relative path, based on {@link Class} position in file system
     * @param target class that is returned from deserialization.
     * @param mainclass class that is used to determine true relative path. You should always specify application`s mainclass to get a proper relative path
     * @return An object, that was read from JSON file and deserialized based on the target class
     * @throws FileNotFoundException if the absolutePath does not correspond to a file.
     * @throws JsonSyntaxException if the file is not a proper JSON
     * @throws JsonIOException if the file cannot be deserialized to a target class
     */
    public static <T> T getJsonObjectWithRelativeFilePath(String relativePath, Class<T> target, Class<?> mainclass)
            throws FileNotFoundException, JsonSyntaxException, JsonIOException {
        log.info("Json path: {}", Utils.getRelativeFilepath(mainclass) + relativePath);
        return JsonReader.getObject(Utils.getRelativeFilepath(mainclass) + relativePath, target);
    }

    /**
     * Opens a {@link File} and deserializes it into an {@link Object} based on target {@link Class}.
     *
     * @param relativePath relative path to file, e.g. "myfile.json". Returns relative path, based on relativity parameter
     * @param target class that is returned from deserialization.
     * @param mainclass class that is used to determine true relative path. You should always specify application`s mainclass to get a proper relative path
     * @param relativity boolean parameter, that decides which method of determining relative path to use - true relative path, or approximate one
     * @return An object, that was read from JSON file and deserialized based on the target class
     * @throws FileNotFoundException if the absolutePath does not correspond to a file.
     * @throws JsonSyntaxException if the file is not a proper JSON
     * @throws JsonIOException if the file cannot be deserialized to a target class
     */
    public static <T> T getJsonObjectWithSwitchableRelativeFilePath(String relativePath, Class<T> target, Class<?> mainclass, boolean relativity)
            throws FileNotFoundException, JsonSyntaxException, JsonIOException {
        return relativity ?
                getJsonObjectWithRelativeFilePath(relativePath, target, mainclass)
                :
                getJsonObjectWithApproximateRelativeFilePath(relativePath, target);
    }

    /**
     * Serializes an {@link Object} into UTF-8 {@link String} and writes into a file (clearing the file before that)
     *
     * @param absolutePath absolute path to file, e.g. "./myfile.json".
     * @param object class that is serialized
     */
    public static void saveJsonObject(String absolutePath, Object object){
        log.info("Json path: {}", absolutePath);
        JsonReader.saveObject(absolutePath, object);
    }

    /**
     * Serializes an {@link Object} into UTF-8 {@link String} and writes into a file (clearing the file before that)
     *
     * @param relativePath relative path to file, e.g. "myfile.json". May not be an actual one, uses default path addressing
     * @param object class that is serialized
     */
    public static void saveJsonObjectWithApproximateRelativeFilePath(String relativePath, Object object){
        log.info("Json path: {}", Utils.getRelativeFilepath() + relativePath);
        JsonReader.saveObject(Utils.getRelativeFilepath()+relativePath, object);
    }

    /**
     * Serializes an {@link Object} into UTF-8 {@link String} and writes into a file (clearing the file before that)
     *
     * @param relativePath relative path to file, e.g. "myfile.json". Returns relative path, based on {@link Class} position in file system
     * @param object class that is serialized
     * @param mainclass class that is used to determine true relative path. You should always specify application`s mainclass to get a proper relative path
     */
    public static void saveJsonObjectWithRelativeFilePath(String relativePath, Object object, Class<?> mainclass){
        log.info("Json path: {}", Utils.getRelativeFilepath(mainclass) + relativePath);
        JsonReader.saveObject(Utils.getRelativeFilepath(mainclass)+relativePath, object);
    }

    /**
     * Serializes an {@link Object} into UTF-8 {@link String} and writes into a file (clearing the file before that)
     *
     * @param relativePath relative path to file, e.g. "myfile.json". Returns relative path, based on relativity parameter
     * @param object class that is serialized
     * @param mainclass class that is used to determine true relative path. You should always specify application`s mainclass to get a proper relative path
     * @param relativity boolean parameter, that decides which method of determining relative path to use - true relative path, or approximate one
     */
    public static void saveJsonObjectWithSwitchableRelativeFilePath(String relativePath, Object object, Class<?> mainclass, boolean relativity) {
        if (relativity) {
            saveJsonObjectWithRelativeFilePath(relativePath, object, mainclass);
        } else {
            saveJsonObjectWithApproximateRelativeFilePath(relativePath, object);
        }
    }
}
